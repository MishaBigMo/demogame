//
//  GameScene.swift
//  the game 2
//
//  Created by Михаил on 27/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let personCategory: UInt32 = 0x1 << 0
    let badObjectCategory: UInt32 = 0x1 << 1
    let goodObjectCategory: UInt32 = 0x1 << 2
    var scoreLabel = SKLabelNode()
    var healthLabel = SKLabelNode()
    
    // properties
    let velocitySpeedX: CGFloat = 100.0
    var scaleData: CGFloat = 100
    var badObjectPerSecond: Double = 1
    var goodObjectPerSecond: Double = 2.5
    var healthObjectPerSecond: Double = 0.12

    //1 создаем экземпляр node
    var person: SKSpriteNode!
    var score: Int = 0
    var hp: Int = 3
    
    override func didMove(to view: SKView) { // вызывается при запуске игры
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector(dx: 0.0, dy: -0.8)
        
        healthLabel = SKLabelNode(text: "Health: \(hp)")
        healthLabel.fontSize = 85
        healthLabel.fontColor = .black
        
        healthLabel.position = CGPoint(x: 0, y: 520)
        self.addChild(healthLabel)
        
        // create ScoreLabel
        scoreLabel = SKLabelNode(text: "Score: \(score)")
        scoreLabel.fontSize = 85
        scoreLabel.fontColor = .black
        
        scoreLabel.position = CGPoint(x: 0, y: 440)
        self.addChild(scoreLabel)
        //2 init node
        person = SKSpriteNode(imageNamed: "car")
        person.position = CGPoint(x: 0, y: -500)
        person.scale(to: CGSize(width: 100, height: 100))
        person.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 75, height: 85))//(texture: person.texture!, size: person.size)
        person.physicsBody?.isDynamic = false
        person.name = "person"
        
        person.physicsBody?.categoryBitMask = personCategory
        person.physicsBody?.collisionBitMask = badObjectCategory
        person.physicsBody?.contactTestBitMask = badObjectCategory

        self.addChild(person)
        
        // generation badObject
        let goodObjectCreate = SKAction.run {
            let goodObject = self.createGoodObjects()
            self.addChild(goodObject)
        }

        
        let healthObjectCreate = SKAction.run {
            let healthObject = self.createHealthObjects()
            self.addChild(healthObject)
        }
        let healthObjectCreateDelay = SKAction.wait(forDuration: 1.0 / healthObjectPerSecond, withRange: 0.5)
        let healthObjectSequenceAction = SKAction.sequence([healthObjectCreate, healthObjectCreateDelay])
        let healthObjectRunActon = SKAction.repeatForever(healthObjectSequenceAction)
        run(healthObjectRunActon)
        
        let goodObjectCreateDelay = SKAction.wait(forDuration: 1.0 / goodObjectPerSecond, withRange: 0.5)
        let goodObjectSequenceAction = SKAction.sequence([goodObjectCreate, goodObjectCreateDelay])
        let goodObjectRunActon = SKAction.repeatForever(goodObjectSequenceAction)
        run(goodObjectRunActon)
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            //3 определяем точку прикосновения
            let touchLocation = touch.location(in: self)
            print(touchLocation)
            
            let distance = distanceCalc(a: person.position, b: touchLocation)
            let speed: CGFloat = 500
            let time = timeToTravelDistance(distance: distance, speed: speed)
            
            //4 создаем дейтсвие
            let moveAction = SKAction.move(to: touchLocation, duration: 0.4)
            print("time: \(time)")
            print("distance \(distance)")
            
            
            person.run(moveAction)
        }
    }
    
    func pauseButtonPressed(sender: AnyObject) {
        if !isPaused {
            pauseGame()
        } else {
            unpause()
        }
    }
    
    func unpause() {
        isPaused = false
    }
    func pauseGame() {
        isPaused = true
    }
    func resetTgeGame() {
        score = 0
        scoreLabel.text = "Score: \(score)"
        isPaused = false
    }
    
    
    func distanceCalc(a: CGPoint, b: CGPoint) -> CGFloat {
        return sqrt((b.x - a.x)*(b.y - a.y) + (b.y - a.y)*(b.y - a.y))
    }
    
    func timeToTravelDistance(distance: CGFloat, speed: CGFloat) -> TimeInterval {
        let time = distance / speed
        return TimeInterval(time)
    }
    
    func createGoodObjects() -> SKSpriteNode {
        let goodObject = SKSpriteNode(color: .yellow, size: CGSize(width: 85, height: 85))
        
        goodObject.position.x = CGFloat(GKRandomSource.sharedRandom().nextInt(upperBound: 6))
        
        goodObject.position.y = frame.size.height + goodObject.size.height
        
        goodObject.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 85, height: 85))
        
        goodObject.name = "GoodObj"
        
        goodObject.physicsBody?.categoryBitMask = badObjectCategory
        goodObject.physicsBody?.collisionBitMask = personCategory
        goodObject.physicsBody?.contactTestBitMask = personCategory
        
        goodObject.physicsBody?.angularVelocity = CGFloat(drand48() * 2 - 1) * 3
        goodObject.physicsBody?.velocity.dx = CGFloat(drand48() * 2 - 1) * velocitySpeedX
        
        return goodObject
    }
    
    func createHealthObjects() -> SKSpriteNode {
        let healthObject = SKSpriteNode(color: .green, size: CGSize(width: 85, height: 85))
        
        healthObject.position.x = CGFloat(GKRandomSource.sharedRandom().nextInt(upperBound: 6))
        
        healthObject.position.y = frame.size.height + healthObject.size.height
        
        healthObject.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 85, height: 85))
        
        healthObject.name = "healthObj"
        
        healthObject.physicsBody?.categoryBitMask = badObjectCategory
        healthObject.physicsBody?.collisionBitMask = personCategory
        healthObject.physicsBody?.contactTestBitMask = personCategory
        
        healthObject.physicsBody?.angularVelocity = CGFloat(drand48() * 2 - 1) * 3
        healthObject.physicsBody?.velocity.dx = CGFloat(drand48() * 2 - 1) * velocitySpeedX
        return healthObject
    }
    
    func createBadObjects() -> SKSpriteNode{
        let badObject = SKSpriteNode(color: .red, size: CGSize(width: 85, height: 85))
        
        badObject.position.x = CGFloat(GKRandomSource.sharedRandom().nextInt(upperBound: 6))
        
        badObject.position.y = frame.size.height + badObject.size.height
        
        badObject.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 85, height: 85))
        
        badObject.name = "badObj"
        
        badObject.physicsBody?.categoryBitMask = badObjectCategory
        badObject.physicsBody?.collisionBitMask = personCategory
        badObject.physicsBody?.contactTestBitMask = personCategory
        
        badObject.physicsBody?.angularVelocity = CGFloat(drand48() * 2 - 1) * 3
        badObject.physicsBody?.velocity.dx = CGFloat(drand48() * 2 - 1) * velocitySpeedX
        
    
        
        return badObject
    }
    
    override func didSimulatePhysics() {
        let hightScreen = UIScreen.main.bounds.height
        enumerateChildNodes(withName: "GoodObj") { (goodObj, stop) in
            if goodObj.position.y < -hightScreen {
                goodObj.removeFromParent()
            }
        }
        enumerateChildNodes(withName: "badObj") { (badObject, stop) in
            if badObject.position.y < -hightScreen {
                badObject.removeFromParent()
            }
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if contact.bodyA.node?.name == "person" && contact.bodyB.node?.name == "GoodObj" {
            print("good")
            self.score += 1
            self.scoreLabel.text = "Score: \(self.score)"
            scaleData *= CGFloat(1.05)
            person.scale(to: CGSize(width: scaleData, height: scaleData))
            
            switch score {
            case 1:
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 10:
                badObjectPerSecond = 1.25
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 20:
                badObjectPerSecond = 1.75
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 30:
                badObjectPerSecond = 2.25
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 40:
                badObjectPerSecond = 2.75
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 50:
                badObjectPerSecond = 3.25
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 60:
                badObjectPerSecond = 3.75
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 70:
                badObjectPerSecond = 4
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 80:
                badObjectPerSecond = 4.25
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            case 90:
                badObjectPerSecond = 4.5
                let badObjectCreate = SKAction.run {
                    let badObject = self.createBadObjects()
                    self.addChild(badObject)
                }
                let badObjectCreateDelay = SKAction.wait(forDuration: 1.0 / badObjectPerSecond, withRange: 0.5)
                let badObjectSequenceAction = SKAction.sequence([badObjectCreate, badObjectCreateDelay])
                let badObjectRunActon = SKAction.repeatForever(badObjectSequenceAction)
                run(badObjectRunActon)
            default:
                print(score)
            }
            contact.bodyB.node?.removeFromParent()
            
        } else if contact.bodyA.node?.name == "person" && contact.bodyB.node?.name == "badObj" {
            self.hp -= 1
            self.healthLabel.text = "Health: \(self.hp)"
            
            // createAnimation
            let colorAction1 = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.3)
            let colorAction2 = SKAction.colorize(with: .white, colorBlendFactor: 0, duration: 0.3)
            let sequence = SKAction.sequence([colorAction1, colorAction2])
            self.person.run(sequence)
            
            //deleteNode
            contact.bodyB.node?.removeFromParent()
            print("bad")
        
            if hp == 0 {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "GameOverViewController") as! GameOverViewController
                controller.delegate = self
                
                view!.addSubview(controller.view)
                controller.view.frame = view!.bounds
               
                controller.view.alpha = 0
                UIView.animate(withDuration: 0.2) {
                    controller.view.alpha = 1
                }
                
                isPaused = true
            }
        } else if contact.bodyA.node?.name == "person" && contact.bodyB.node?.name == "healthObj" {
            self.hp += 1
            self.healthLabel.text = "Health: \(self.hp)"

            scaleData *= CGFloat(0.5)
            person.scale(to: CGSize(width: scaleData, height: scaleData))
            contact.bodyB.node?.removeFromParent()

        }
    }

    func didEnd(_ contact: SKPhysicsContact) {
        
    }
    
}
