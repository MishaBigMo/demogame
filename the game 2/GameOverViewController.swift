//
//  GameOverViewController.swift
//  the game 2
//
//  Created by Михаил on 30/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import SpriteKit
import UIKit

class GameOverViewController: UIViewController {
    
    var delegate: GameScene?

    @IBOutlet weak var highScore: UILabel!
    @IBOutlet weak var score: UILabel!
    
    @IBOutlet weak var tryAgainButton: RoundButton!
    
    @IBAction func tryAgainTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)

    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let scores = delegate?.score {
            score.text = "You score: \(scores)"
        }

    }
    

}
