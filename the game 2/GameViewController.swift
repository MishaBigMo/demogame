//
//  GameViewController.swift
//  the game 2
//
//  Created by Михаил on 27/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

var gameScene: GameScene!
var pauseVC: PauseViewController!
var gameOverViewController: GameOverViewController!

class GameViewController: UIViewController {
    

    
    @IBAction func pauseTapped(_ sender: UIButton) {
        gameScene.pauseButtonPressed(sender: sender)
        showVC(pauseVC)
    }
    
    func gameOver() {
        var vc: GameOverViewController!
        vc = storyboard?.instantiateViewController(withIdentifier: "GameOverViewController") as! GameOverViewController
        view.addSubview(vc.view)
        vc.view.frame = view.bounds
    }
    
    func showVC(_ vc: UIViewController) {
        addChild(vc)
        view.addSubview(vc.view)
        vc.view.frame = view.bounds
        
        vc.view.alpha = 0
        
        UIView.animate(withDuration: 0.2) {
            vc.view.alpha = 1
        }
    }
    
    func hidePauseVC(vc: PauseViewController) {
        vc.willMove(toParent: nil)
        vc.removeFromParent()
        
        vc.view.alpha = 1
        
        UIView.animate(withDuration: 0.2, animations: {
            vc.view.alpha = 0
        }) { (completed) in
            vc.view.removeFromSuperview()
        }
        gameScene.isPaused = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pauseVC = storyboard?.instantiateViewController(withIdentifier: "PauseViewController") as! PauseViewController
        pauseVC.delegate = self
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "GameScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                gameScene = scene as! GameScene
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}


extension GameViewController: PauseVCDelegate {
    
    func pauseVCPlayButton(_ viewController: PauseViewController) {
        hidePauseVC(vc: pauseVC)
    }
    
    
}
