//
//  PauseViewController.swift
//  the game 2
//
//  Created by Михаил on 30/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

protocol PauseVCDelegate {
    func pauseVCPlayButton(_ viewController: PauseViewController)
}

class PauseViewController: UIViewController {
    
    var delegate: PauseVCDelegate!

    @IBAction func resetTapped(_ sender: Any) {
        
        
    }
    @IBAction func countinueTapped(_ sender: Any) {
        delegate.pauseVCPlayButton(self)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

}
